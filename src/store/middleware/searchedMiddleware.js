import { profileAPI } from "../../components/Profile/profileAPI";
import { ACTION_SEARCHED_FETCHING, searchedErrorAction, searchedSetAction  } from "../actions/searchedActions"

export const searchedMiddleware = ({ dispatch }) => next => action => {
    next(action)

    if(action.type === ACTION_SEARCHED_FETCHING) {
        profileAPI.searched(action.payload)
        .then(searched => {
            dispatch(searchedSetAction(searched))
        })
        .catch(error => {
            dispatch(searchedErrorAction(error.message))
        })
    }
}