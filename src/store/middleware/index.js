import { applyMiddleware } from 'redux'
import { searchedMiddleware } from './searchedMiddleware'
import { translationMiddleware } from './translationMiddleware'

export default applyMiddleware(
    translationMiddleware,
    searchedMiddleware
)