import { API } from "../../components/Home/API"
import { ACTION_TRANSLATION_ATTEMPTING, translationErrorAction, translationSuccessAction } from "../actions/translationActions"

export const translationMiddleware = ({ dispatch }) => next => action => {
    next(action)

    if(action.type === ACTION_TRANSLATION_ATTEMPTING) {
        API.translation(action.payload)
        .then(word => {
            dispatch( translationSuccessAction(word))
        })
        .catch(error => {
            dispatch(translationErrorAction(error.message))
        })
    }
}