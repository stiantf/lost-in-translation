import { combineReducers } from 'redux'
import { searchedReducer } from './searchedReducers'
import { translationReducer } from './translationReducer'

const appReducer= combineReducers({
    translationReducer,
    searchedReducer
})

export default appReducer