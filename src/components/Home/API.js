export const API = {
    //API request to the db.json file
    translation(word) {
        return fetch(`http://localhost:8000/signs/${word}`, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(async (response) => {
            //Error happend
            if(!response.ok) {
                const { error = 'Could not find translation'} = await response.json()
                throw new Error(error)
            }
            //No error
            return response.json()
        })
    }
}