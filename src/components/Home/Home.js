import { useAuth0 } from "@auth0/auth0-react";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import AppContainer from "../../hoc/AppContainer";
import { translationAttemptAction } from "../../store/actions/translationActions";
import Navbar from "../Navbar/Navbar";
import { API } from "./API";
import styles from "./Home.module.css";

// Home screen after you login! Here you can translate words
function Home() {
  const { isAuthenticated, isLoading } = useAuth0();

  const dispatch = useDispatch();
  const { translationError, translationAttempting } = useSelector(
    (state) => state.translationReducer
  );

  const [word, setWord] = useState({
    search: "",
  });

  const [state, setState] = useState({
    translation: [],
    fetching: true,
    error: "",
  });

  //State of the input field
  const onInputChange = (event) => {
    setWord({
      ...word,
      [event.target.id]: event.target.value.trim(),
    });
  };

  //When the search-button is pressed this action commits
  const onFormSubmit = (event) => {
    event.preventDefault(); // stopping the page reload
    dispatch(translationAttemptAction(word.search));
    API.translation(word.search)
      .then((translation) => {
        setState({
          ...state,
          fetching: false,
          translation: [translation],
        });
        // console.log(translation);
      })
      .catch((error) => {
        setState({
          ...state,
          fetching: false,
          error: error.message,
        });
      });
  };

  // Showing a loadingicon when loading into the component
  if (isLoading) {
    return (
      <>
        <Navbar />
        <div className="d-flex justify-content-center">
          <div className="spinner-border mt-5" role="status">
            <span className="sr-only"></span>
          </div>
        </div>
      </>
    );
  }

  return (
    // Showing the site aslong as the user is authenticated.
    isAuthenticated && (
      <>
        <Navbar />

        <AppContainer>
          <main className="Home">
            <h1>Lost in translation</h1>
            <h4 className="mt-5">Get started:</h4>
            <form onSubmit={onFormSubmit}>
              <div className="form-row align-items-center mt-1">
                <div className="col-sm-4 my-2 col">
                  <input
                    type="text"
                    className="form-control"
                    id="search"
                    placeholder="Enter translation"
                    onChange={onInputChange}
                  />
                </div>
                <div className="col-sm-3 my-1 col">
                  <button type="submit" className="btn btn-primary">
                    Search
                  </button>
                </div>
              </div>
            </form>

            {/* Here comes the translation */}
            <div className="mt-5">
              <h4>Translation:</h4>
              <div className={styles.Translations}>
                {state.translation.map((translation) => (
                  <div key={translation.id}>
                    <img
                      src={translation.url}
                      alt={translation.id}
                      className={styles.TranslationImage}
                    />
                  </div>
                ))}
              </div>
            </div>
          </main>
          {/* Showing the user that the translation is loading */}
          {translationAttempting && (
            <div className="d-flex justify-content-center">
              <div className="spinner-border mt-5" role="status">
                <span className="sr-only"></span>
              </div>
            </div>
          )}

          {/* Displaying the error to the user*/}
          {translationError && (
            <div className="alert alert-danger" role="alert">
              <h4>Translation Failed</h4>
              <p>{translationError}</p>
            </div>
          )}
        </AppContainer>
      </>
    )
  );
}

export default Home;
