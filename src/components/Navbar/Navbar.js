function Navbar() {
  return (
    //Simple navbar using bootstrap
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <span className="navbar-text">Lost in Translation</span>
      <div id="navbarNavAltMarkup">
        <div className="navbar-nav">
          <a className="nav-item nav-link" href="/home" >
            Home
          </a>
          <a className="nav-item nav-link" href="/profile">
            Profile
          </a>
        </div>
      </div>
    </nav>
  );
}

export default Navbar;
