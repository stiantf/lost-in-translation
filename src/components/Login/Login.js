import { useAuth0 } from "@auth0/auth0-react";
import { Redirect } from "react-router-dom";
import AppContainer from "../../hoc/AppContainer";
import logo from "../../images/logo.png";

//Login function using Auth0
function Login() {
  const { loginWithRedirect, isAuthenticated, isLoading } = useAuth0();

  //Showinging loadinc icon is isLoading is true
  if (isLoading) {
    return (
      <div className="d-flex justify-content-center">
        <div className="spinner-border mt-5" role="status">
          <span className="sr-only"></span>
        </div>
      </div>
    );
  }

  return (
    <AppContainer>
      {/* Only displaying the content if isAuthenticated is true */}
      {isAuthenticated && <Redirect to="/home" />}
      <main className="text-center">
        <h1 className="mb-5">Lost in Translation</h1>
        <h4>Click on the button below to login/register</h4>
        <button className="btn btn-primary" onClick={() => loginWithRedirect()}>
          Login/Register
        </button>
      </main>
      <img
        src={logo}
        alt="Welcome"
        className="img-responsive center-block d-block mx-auto w-25 mt-5"
      />
    </AppContainer>
  );
}

export default Login;
