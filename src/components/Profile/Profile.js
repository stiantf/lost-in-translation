import { useAuth0 } from "@auth0/auth0-react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import AppContainer from "../../hoc/AppContainer";
import Navbar from "../Navbar/Navbar";
import { searchedFetchingAction } from "../../store/actions/searchedActions"

function Profile() {
  const { logout, isAuthenticated, user, isLoading } = useAuth0();

 // const { profile } = useSelector(state => state.profile)
  //const { session } = useSelector(state => state.session)
 // const dispatch = useDispatch()

 /* useEffect(() => {
    dispatch(searchedFetchingAction(user.email))
  }, [dispatch, session]) */

  // Showing a loadingicon when loading into the component
  if (isLoading) {
    return (
      <>
      <Navbar/>
        <div className="d-flex justify-content-center">
          <div className="spinner-border mt-5" role="status">
            <span className="sr-only"></span>
          </div>
        </div>
      </>
    );
  }

  return (
    isAuthenticated && (
      <>
        <Navbar />
        <AppContainer>
          <main className="Login">
            <h1>You are viewing your profile</h1>
            <div className="row">
              {/* First column */}
              <div className="col">
                <h2>{user.name}</h2>
            <p>{user.email}</p>
            <button
              onClick={() => logout({ returnTo: "http://localhost:3000/" })}
            >
              Logout
            </button>
              </div>

               {/* Second column */}
              <div className="col">
                <h4>Previously searched translations:</h4>
                <section>
                 {/* SHOULD SHOW 10 OF THE PREVIOUSLY SEARCHED WORDS
                 {profile.map((searched) => (
                    <p>{searched.searched}</p>
                 ))} */}
                </section>

              </div>

            </div>
            
          </main>
        </AppContainer>
      </>
    )
  );
}

export default Profile;
