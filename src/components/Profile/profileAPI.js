export const profileAPI = {
    //Getting the user data based on the user ID
    searched(user) {
        return fetch(`http://localhost:8000/users/${user}`, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(async (response) => {
            //Error happend
            if(!response.ok) {
                const { error = 'Could not find previous translations'} = await response.json()
                throw new Error(error)
            }
            //No errror
            return response.json()
        })
    }
}