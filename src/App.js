import "./App.css";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Login from "./components/Login/Login";
import NotFound from "./components/NotFound/NotFound";
import Home from "./components/Home/Home";
import Profile from "./components/Profile/Profile";

function App() {
  return (
    <BrowserRouter>
      <div className="App">

        <Switch>
          <Route path="/" exact component={ Login } />
          <Route path="/home" component={ Home } />
          <Route path="/profile" component={ Profile } />
          <Route path="*" component={ NotFound } />
          
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
