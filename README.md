# Lost in translation

Application created with React.
Database is hosted local with json-server
json file is db.json
Bootstrap used for styling.



## Login
Authenticate with Auth0

Login with Auth0 account or google account

## Navbar
Navigate between Home and Profile

## Home
Type your word and translate it to sign langugage
As for now the translation only works for single letters. 
The original idea was to split the input word into seperate letters and then display the sign language images based on these letters.

With the current solution you have to add the word and urls into the database. For now "yes" is added. This solution is not optimal and there is also a problem that it only displays the first image in the imageurl array.

## Profile
See your profile info.
Logout from the application

(Should also include a list of your last 10 translated words, but I didnt have enough time to implement it).







